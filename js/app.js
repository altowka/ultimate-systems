/////////////////////////////////////////dropdown

//remove dropdown if you click outside
$(window).click(function (e) {
    if (!(myDrop.contains(e.target))) {
        $("#myDropdown").removeClass("show");
    }
})

//insert link text in dropdown input and change view of cliecked link
$('.man').click(function () {
    // get the contents of the link that was clicked
    let linkText = $(this).text();
    // replace the contents of the div with the link text
    $('.dropbtn').val(linkText);
    //added blue class to clicked items and remove from the rest
    $('.man').removeClass("blue-class");
    $(this).addClass("blue-class");

});

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
$('.dropbtn').click(function () {
    $("#myDropdown").toggleClass("show");
});


//filter in vanilla JS
function filterFunction() {
    let input, filter, a, div;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    div = document.getElementById("myDropdown");
    a = div.getElementsByTagName("a");
    for (let i = 0; i < a.length; i++) {
        txtValue = a[i].textContent || a[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}


///////////////////////////////////task

//removing row
$('body').on('click', '.remove', function (e) {
    e.target.closest('tr').remove();
});


let lolz = document.getElementById('lolz'); //task-name
let price = document.getElementById('price');
let convert = document.getElementById('convert').innerText;


//adding new row
$('.add-button').click(function (e) {
    //validation
    if (lolz.value.length < 5 && price.value.length === 0) {
        $("#lolz-box").addClass("first-error");
        $("#price-box").addClass("first-error");
        // $(".first-error::after").style("content", "byhaha")
    } else if (lolz.value.length < 5) {
        $("#price-box").removeClass("first-error");
        $("#lolz-box").addClass("first-error");
    } else if (price.value.length === 0) {
        $("#lolz-box").removeClass("first-error");
        $("#price-box").addClass("first-error");
    }
    //removing
    else {
        $("#price-box").removeClass("first-error");
        $("#lolz-box").removeClass("first-error");
        $(".task__table>tbody").append(`<tr>
    <td>${lolz.value}</td>
        <td class="cash">${numberWithSpaces(price.value)} PLN</td>
        <td>${numberWithSpaces(Math.round((price.value / convert) * 100) / 100) } EUR</td>
        <td class="remove">Usuń</td>
    </tr>`);

    }

});

//adding spaces to numbers
function numberWithSpaces(x) {
    let parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}


//////////////////////////sum

let nodeCash = document.getElementsByClassName("cash");

setInterval(() => {

    let sum = 0;
    for (let i = 0; i < nodeCash.length; i++) {
        let numb = nodeCash[i].innerText.replace("PLN", "");
        let numb2 = numb.replace(" ", "");
        sum += Number(numb2);
    }

    document.getElementById('sum-zl').innerText = numberWithSpaces(sum);
    document.getElementById('sum-eur').innerText = numberWithSpaces(Math.round((sum / convert) * 100) / 100);

}, 1);


//////////////////////////////sorting

//////////////sort table-by number
function sortTable(rowNumb) {
    let tbl = document.getElementById("mytable").tBodies[0];
    let store = [];
    for (let i = 0, len = tbl.rows.length; i < len; i++) {
        let row = tbl.rows[i];
        let sortnr = parseFloat(row.cells[rowNumb].textContent.replace(" ", "") || row.cells[rowNumb].innerText.replace(" ", ""));
        if (!isNaN(sortnr)) store.push([sortnr, row]);
    }
    store.sort(function (x, y) {
        return x[0] - y[0];
    });
    for (let i = 0, len = store.length; i < len; i++) {
        tbl.appendChild(store[i][1]);
    }
    store = null;
}

//////////////sort table-by number-reverse
function sortTableReverse(rowNumb) {
    let tbl = document.getElementById("mytable").tBodies[0];
    let store = [];
    for (let i = 0, len = tbl.rows.length; i < len; i++) {
        let row = tbl.rows[i];
        let sortnr = parseFloat(row.cells[rowNumb].textContent.replace(" ", "") || row.cells[rowNumb].innerText.replace(" ", ""));
        if (!isNaN(sortnr)) store.push([sortnr, row]);
    }
    store.sort(function (x, y) {
        return y[0] - x[0];
    });
    for (let i = 0, len = store.length; i < len; i++) {
        tbl.appendChild(store[i][1]);
    }
    store = null;
}

/////sort table alphabetically

function sortTableAlpha(rowNumb) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("mytable");
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[i + 1].getElementsByTagName("TD")[0];
            // Check if the two rows should switch place:
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

/////sort table alphabetically-reverse
function sortTableAlphaReverse(rowNumb) {
    let table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("mytable");
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[i + 1].getElementsByTagName("TD")[0];
            // Check if the two rows should switch place:
            if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

///////////////////////////////validation 

let companyName = document.getElementById("company-name");

companyName.addEventListener('blur', () => {
    if (companyName.value === '') {
        $('.company-name-box').addClass("first-error");
    } else {
        $('.company-name-box').removeClass("first-error");
    }
});


let dropButton = document.getElementById("drop-button");
let myDrop = document.getElementById("drop-down-box");


// validation employee input
$('.dropbtn').focusout(function (e) {
    if (dropButton.innerText === '' || dropButton.innerText === 'Pracownik') {
        $('.drop-button-box').addClass("first-error");
    }
})

// remove error if choose someone from the list of people or if you're typing in search input
$('#myDropdown').focusin(function () {
    $('.drop-button-box').removeClass("first-error");
})


/////////////////add inputs label

$(document).ready(function () {
    $(".input-action").on("keyup keydown", function (event) {
        let element_id = $(this).attr('data-show-place');
        let target = ".item-" + element_id;
        let value = $(this).val();
        let text = this.getAttribute('placeholder');
        $(target).text(text)
        $(target).show();
        if (value == "") {
            $(target).hide();
        }
    });

    ////label color to blue
    $('.input-action').focus(function () {
        let element_id = $(this).attr('data-show-place');
        let target = ".item-" + element_id;
        $(target).css("color", "#00447C");
    })

    ////label color to gray
    $('.input-action').blur(function () {
        let element_id = $(this).attr('data-show-place');
        let target = ".item-" + element_id;
        $(target).css("color", "#828282");
    })

    //add label to employee input when user pick someone
    $(".man").click(function () {
        $("#drop-button").attr("placeholder", "");
        $(".item-4").show();
    })

});